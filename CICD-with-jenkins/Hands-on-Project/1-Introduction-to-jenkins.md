## Getting Started With Jenkins

Now that we have an idea what jenkins, let's dive in to installing jenkins

**Update package repositories**

      sudo apt update
***Install JDK***

    sudo apt install default-jdk-headless
***Install Jenkins***

    wget -q -O - https://pkg.jenkins.io/debian-stable/jenkins.io.key | sudo apt-key add -
    sudo sh -c 'echo deb https://pkg.jenkins.io/debian-stable binary/ > \
    /etc/apt/sources.list.d/jenkins.list'
    sudo apt update
    sudo apt-get install jenkins
***Check if jenkins has been installed, and it is up and running***

    sudo systemctl status jenkins

 ![jenkins](../images/jenkins-status.PNG)
 

***On our Jenkins instance, create new inbound rules for port 8080 in security group***
![inbound](images/inbound.PNG)


***Set up Jenkins On The Web Console***

i. Input your Jenkins Instance ip address on your web browser i.e. http://public_ip_address:8080


ii. On your Jenkins instance, check  "/var/lib/jenkins/secrets/initialAdminPassword" to know your password.


<table>
  <tr>
    <td><img src="../images/jenkins-password.PNG" alt="Image 1"></td>
    <td><img src="../images/jenkins-password1.PNG" alt="Image 2"></td>
  </tr>
</table>


iii. Installed suggested plugins

![jenkins]../(images/jenkins-plugins.PNG)

iv. Create a user account 

![jenkins](../images/jenkins-admin.PNG)

v. Log in to jenkins console

![jenkins](../images/jenkins-start.PNG)


