## Working with Docker Images

### Introduction to Docker Images

Docker images are the building blocks of containers. They are lightweight, portable, and self-sufficient packages that contain everything needed to run a software application, including the code, runtime, libraries, and system tools. Images are created from a set of instructions known as a Dockerfile, which specifies the environment and configuration for the application.

### Pulling Images from Docker Hub

[Docker Hub](https://hub.docker.com) is a cloud-based registry that hosts a vast collection of Docker images. You can pull images from Docker Hub to your local machine using the `docker pull` command.

To explore available images on Docker Hub, the docker command provides a search subcommand. For instance, to find the Ubuntu image, you can execute:

```bash
docker search ubuntu
```

This command allows you to discover and explore various images hosted on Docker Hub by providing relevant search results. In this case, the output will be similar to this:

![Docker Search](docker/docker-search.PNG)

In the "OFFICIAL" column, the "OK" designation signifies that an image has been constructed and is officially supported by the organization responsible for the project. Once you have identified the desired image, you can retrieve it to your local machine using the "pull" subcommand.

To download the official Ubuntu image to your computer, use the following command:

```bash
docker pull ubuntu
```

Executing this command will fetch the official Ubuntu image from Docker Hub and store it locally on your machine, making it ready for use in creating containers.

![Pull Ubuntu Image](docker/pull-ubuntu.PNG)

Once an image has been successfully downloaded, you can proceed to run a container using that downloaded image by employing the "run" subcommand. Similar to the hello-world example, if an image is not present locally when the `docker run` subcommand is invoked, Docker will automatically download the required image before initiating the container.

To view the list of images that have been downloaded and are available on your local machine, enter the following command:

```bash
docker images
```

Executing this command provides a comprehensive list of all the images stored locally, allowing you to verify the presence of the downloaded image and gather information about its size, version, and other relevant details.

![Docker Images](docker/ubuntu-image.PNG)


As we move on in this course, you will the images to work with containers


### Dockerfile

A Dockerfile is a plaintext configuration file that contains a set of instructions for building a Docker image. It serves as a blueprint for creating a reproducible and consistent environment for your application. Dockerfiles are fundamental to the containerization process, allowing you to define the steps to assemble an image that encapsulates your application and its dependencies.

### Creating a Dockerfile

To create a Dockerfile, use a text editor of your choice, such as vim or nano. Start by specifying a base image, defining the working directory, copying files, installing dependencies, and configuring the runtime environment.

Here's a simple example of a Dockerfile for a html file:
 Let's create an image with using a dockerfile. Paste the code snippet below in a file named `dockerfile`
This example assumes you have a basic HTML file named `index.html` in the same directory as your Dockerfile.

```Dockerfile
# Use the official NGINX base image
FROM nginx:latest

# Set the working directory in the container
WORKDIR  /usr/share/nginx/html/

# Copy the local HTML file to the NGINX default public directory
COPY index.html /usr/share/nginx/html/

# Expose port 80 to allow external access
EXPOSE 80

# No need for CMD as NGINX image comes with a default CMD to start the server
```

Explanation of the code snippet above

1. **FROM nginx:latest:** Specifies the official NGINX base image from Docker Hub.

2. **WORKDIR /usr/share/nginx/html/:** Specifies the working directory in the container

2. **COPY index.html /usr/share/nginx/html/:** Copies the local `index.html` file to the NGINX default public directory, which is where NGINX serves static content from.

3. **EXPOSE 80:** Informs Docker that the NGINX server will use port 80. This is a documentation feature and doesn't actually publish the port.

4. **CMD:** NGINX images come with a default CMD to start the server, so there's no need to specify it explicitly.

HTML file named `index.html` in the same directory as your dockerfile.

```
echo "Welcome to Darey.io" >> index.html
```

To build an image from this Dockerfile, navigate to the directory containing the file and run:

```bash
docker build -t dockerfile .
```

![Dockerfile](docker/docker-file.PNG)


![Docker Images](docker/docekrfile-image.PNG)

To run a container based on the custom NGINX image we created with a dockerfile, run the command

```bash
docker run -p 8080:80 dockerfile
```
![Dcker Run](docker/docker-run.PNG)

![Security Group](docker/security-group.PNG)

**Note:** Running the command above will create a container that listens on port 80 using the nginx image we created earlier. So we need to open port 8080 on AWS Console

Let see the list of available containers

```
docker ps -a
```

![Docker Container](docker/dockerfile-container1.PNG)

The image above show our container is not running yet. We can start it with the command below

```
docker start CONTAINER_ID
```

![Docker Start](docker/docker-start.PNG)


![Docker Container](docker/dockerfile-container.PNG)

Now that we have started our container, we can access the content on our web browser with http://publicip_adderss:8080

![Web Browser](docker/web-browser.PNG)
