## Introduction to Docker and Containers

### What are Containers?

[Docker](https://www.docker.com) is a platform that enables the development, deployment, and execution of applications inside containers. Containers are lightweight, portable, and self-sufficient units that encapsulate an application and its dependencies, ensuring consistent performance across different environments.

Containers provide process and filesystem isolation, allowing applications to run independently of the host system. They encapsulate an application and its dependencies, including libraries, binaries, and configuration files, ensuring a consistent and reproducible environment.

### Advantages of Containers

**Portability Across Different Environments**: Containers ensure consistency across various environments, such as development, testing, and production. They encapsulate all dependencies, making it easier to move applications seamlessly between different systems and cloud providers.

**Resource Efficiency Compared to Virtual Machines**: Containers share the host OS kernel, which reduces overhead and makes them more resource-efficient than virtual machines (VMs). Containers consume fewer resources, leading to faster startup times and better utilization of system resources.

**Rapid Application Deployment and Scaling**: Containers enable agile and scalable application deployment. With container orchestration tools like Docker Compose and Kubernetes, you can easily deploy, scale, and manage applications, ensuring rapid development cycles and efficient resource utilization.

### Comparison with Virtual Machines

#### Differentiating Containers from Virtual Machines
Containers and virtual machines serve similar purposes but differ in their approach. Virtual machines emulate an entire operating system, including a separate kernel, while containers share the host OS kernel. This fundamental difference results in containers being more lightweight and faster to start compared to virtual machines.

#### Resource Utilization and Performance Benefits
Due to their lightweight nature, containers offer improved resource utilization and performance. Containers share the host OS resources, avoiding the need for a full OS emulation, leading to faster deployment, efficient use of resources, and better overall system performance.

Comparison of Docker Container with Virtual Machines

As our narrative unfolds, let us delve into the captivating tale of the age-old rivalry between Docker containers and virtual machines, two powerful entities vying for supremacy in the enchanted realm of IT industry.

**1. The Battle of Weight:**

In the ethereal world of resource consumption, virtual machines (VMs) present themselves as majestic castles, each hosting its own operating system, complete with knights (applications), squires (dependencies), and court jesters (libraries). These grand structures, however, demand a hefty toll on the kingdom's resources.

On the other side of the battlefield stands Docker, with its containers akin to nimble carriages. Containers share the host operating system's kernel, making them lightweight and efficient. Picture this: while a virtual machine might resemble a majestic dragon in flight, Docker containers flit through the air like agile fairies, requiring minimal resources and leaving the kingdom's coffers intact.

**2. The Quest for Speed:**

As our heroes embark on quests of deployment, the difference in speed becomes apparent. Virtual machines, with their hefty armor, take time to boot up and initialize. The journey from castle gate to the battleground is a slow march, laden with the weight of the entire virtualized operating system.

In contrast, Docker containers are swift and nimble, ready for battle in the blink of an eye. Containers share the host OS kernel, eliminating the need for a full operating system boot-up. It's as if the containers teleport onto the battlefield, leaving virtual machines in their dust. Docker emerges victorious in the quest for speed, ensuring that applications deploy with the agility of a seasoned adventurer.


**3. The Chronicle of Resource Utilization:**

The battle for resource efficiency rages on, and Docker emerges as the champion. Virtual machines, with their elaborate structures, demand more disk space and memory. Each VM hoards resources, leading to inefficiencies in the infrastructure.

Docker containers, however, are frugal with their demands. They share resources, creating a harmonious ecosystem where multiple containers peacefully coexist on a single host. Docker's economy of scale turns the tide in its favor, ensuring that resources are allocated judiciously.


### Importance of Docker

**Technology and Industry Impact:** The significance of Docker in the technology landscape cannot be overstated. Docker and containerization have revolutionized software development, deployment, and management. The ability to package applications and their dependencies into lightweight, portable containers addresses key challenges in software development, such as consistency across different environments and efficient resource utilization.

**Real-World Impact:** Implementing Docker brings tangible benefits to organizations. It streamlines the development process, promotes collaboration between development and operations teams, and accelerates the delivery of applications. Docker's containerization technology enhances scalability, facilitates rapid deployment, and ensures the consistency of applications across diverse environments. This not only saves time and resources but also contributes to a more resilient and agile software development lifecycle.

### Target Audience

This course on Docker is designed for a diverse audience, including:

- **DevOps Professionals:** Interested in container orchestration, seeking efficient ways to manage and deploy applications, improve resource utilization, and ensure system stability.

- **Developers:** Who want to streamline their application development, enhance collaboration, and ensure consistency across different stages of the development lifecycle.
  
It caters to cloud engineers, QA engineers, and other tech enthusiast who are eager to enhance their technical skills and establish a strong foundation in docker and containersation. Professionals seeking to expand their skill set or students preparing for roles in technology-related fields will find this project beneficial.


### Prerequisites
Before diving into this Docker course, learners

- Should have successfully completed our TechOps Career Essentials course.

- Should be comfortable navigating linux and executing its commands.

- Should have basic cloud computing knowledge.

- Understanding of virtual machines and their role in software deployment.



### Project Goals
 
By the end of this course, learners should aim to achieve the following:

1. Grasp the concept of containers, their isolation, and their role in packaging applications.
   
2.  Familiarize themselves with key Docker features, commands, and best practices.
   
3. Comprehend how Docker containers contribute to resource efficiency compared to traditional virtual machines.

4. Learn how Docker ensures consistent application behavior across different development, testing, and production environments.

5. Master the techniques for quickly deploying and scaling applications using Docker.


## Getting Started With Docker

### Installing Docker

We need to launch an ubuntu 20.04 LTS instance and connect to it, then follow the steps below

Before installing Docker Engine for the first time on a new host machine, it is necessary to configure the Docker repository. Following this setup, you can proceed to install and update Docker directly from the repository.

- Set up Docker's apt repository.
```
# Add Docker's official GPG key:
sudo apt-get update
sudo apt-get install ca-certificates curl gnupg
sudo install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Add the repository to Apt sources:
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

- Install latest version of docker
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

- Verify that docker has been successfully installed
```
sudo systemctl status docker
```

![Docker Status](docker/docker-status.PNG)

By default, after installing docker, it can only be run by root user or using `sudo` command. To run the docker command without sudo execute the command below
```
sudo usermod -aG docker $ubuntu
```

After executing the command above, we can run docker command without using superuser privilledges

### Running the "Hello World" Container

**Using the `docker run` Command**

The `docker run` command is the entry point to execute containers in Docker. It allows you to create and start a container based on a specified Docker image. The most straightforward example is the "Hello World" container, a minimalistic container that prints a greeting message when executed.

```bash
# Run the "Hello World" container
docker run hello-world
```

![Hello World](docker/hello-world.PNG)

When you execute this command, Docker performs the following steps:

1. **Pulls Image (if not available locally):** Docker checks if the `hello-world` image is available locally. If not, it automatically pulls it from the Docker Hub, a centralized repository for Docker images.

2. **Creates a Container:** Docker creates a container based on the `hello-world` image. This container is an instance of the image, with its own isolated filesystem and runtime environment.

3. **Starts the Container:** The container is started, and it executes the predefined command in the `hello-world` image, which prints a friendly message.

**Understanding the Docker Image and Container Lifecycle**

**Docker Image:** A Docker image is a lightweight, standalone, and executable package that includes everything needed to run a piece of software, including the code, runtime, libraries, and system tools.
Images are immutable, meaning they cannot be modified once created. Changes result in the creation of a new image.

- **Container Lifecycle:** Containers are running instances of Docker images.
  - They have a lifecycle: `create, start, stop, and delete`.
  - Once a container is created from an image, it can be started, stopped, and restarted.

**Verifying the Successful Execution**

You can check if the images is now in your local environment with
Example Output:

```
docker images
```

![Docker image](docker/hw-image.PNG)

If you encounter any issues, ensure that Docker is properly installed and that your user has the necessary permissions to run Docker commands.

This simple "Hello World" example serves as a basic introduction to running containers with Docker. It helps verify that your Docker environment is set up correctly and provides insight into the image and container lifecycle. As you progress in this course, you'll explore more complex scenarios and leverage Docker for building, deploying, and managing diverse applications.

### Basic Docker Commands

**Docker Run**

The `docker run` command is fundamental for executing containers. It creates and starts a container based on a specified image.

```bash
# Run a container based on the "nginx" image
docker run hello-world
```

This example pulls the "nginx" image from Docker Hub (if not available locally) and starts a container using that image.

**Docker PS**

The `docker ps` command displays a list of running containers. This is useful for monitoring active containers and obtaining information such as container IDs, names, and status.

```bash
# List running containers
docker ps
```

To view all containers, including those that have stopped, add the `-a` option:

```bash
# List all containers (running and stopped)
docker ps -a
```

![Docker Container](docker/dockerps-a.PNG)

**Docker Stop**

The `docker stop` command halts a running container.

```bash
# Stop a running container (replace CONTAINER_ID with the actual container ID)
docker stop CONTAINER_ID
```

**Docker Pull**

The `docker pull` command downloads a Docker image from a registry, such as Docker Hub, to your local machine.

```bash
# Pull the latest version of the "ubuntu" image from Docker Hub
docker pull ubuntu
```

**Docker Push**

The `docker push` command uploads a local Docker image to a registry, making it available for others to pull.

```bash
# Push a local image to Docker Hub
docker push your-username/image-name
```

Ensure you've logged in to Docker Hub using `docker login` before pushing images.

**Docker Images**

The `docker images` command lists all locally available Docker images.

```bash
# List all local Docker images
docker images
```

**Docker RMI**

The `docker rmi` command removes one or more images from the local machine.

```bash
# Remove a Docker image (replace IMAGE_ID with the actual image ID)
docker rmi IMAGE_ID
```

These basic Docker commands provide a foundation for working with containers. Understanding how to run, list, stop, pull, push, and manage Docker images is crucial for effective containerization and orchestration. As you delve deeper into Docker, you'll discover additional commands and features that enhance your ability to develop, deploy, and maintain containerized applications.