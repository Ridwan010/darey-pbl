# LEMP STACK IMPLEMENTATION

# Introduction To LEMP Stack

In project_2 LAMP Stack was implemented, in this project LEMP Stack will be implemented. Learners will understand more about web stack and what LEMP Stack is, its importane, and how it is different from LAMP Stack. As we move forward in this project, learners will learn  how to create and launch web applications using the LEMP stack.

We’ll start by exploring the LEMP stack's architecture. Linux is the strong base, Nginx serves as the powerful web server, MySQL manages databases, and PHP gives life to your web applications by enabling server-side functionality.
In this project, we will understand you more on ec2 instaces, improve the performance of Nginx, manage MySQL databases, and become skilled at developing PHP code for your applications. 

When you finish the Project (LEMP Stack) Course, Get ready to use your imagination and start this exciting adventure with us.
By the end of this project a task will be assigned which will be based on everything we has be done in this project. So let's  start 

### What Is LEMP Stack ?

From the last project, we understand what web stack is a collection of similar things or object (softwares) which work together in an interconnected way to run a function.

The LEMP stack is a popular web development stack that is an alternative to the more traditional LAMP stack. LEMP is an acronym representing the key components of the stack: Linux, Nginx (pronounced "engine-x"), MySQL (or MariaDB), and PHP/Python/Perl. Each component plays a specific role in the web development process.

### Components Of A LEMP Stack
**Linux (Operating System)**: The "L" in LEMP stands for Linux, which serves as the operating system for the server. Linux is chosen for its stability, security, and open-source nature. It provides a reliable foundation for hosting web applications.

**Nginx (Web Server)**: Nginx is a high-performance, open-source web server that is known for its speed and efficiency in handling concurrent connections. It is often used as a reverse proxy server and can serve static content efficiently. Nginx is a key component for optimizing web application performance.

**MySQL (or MariaDB - Database)**: The "M" stands for MySQL, which is a popular open-source relational database management system (RDBMS). Alternatively, MariaDB, a fork of MySQL, is sometimes used. MySQL/MariaDB is responsible for storing and managing the application's data in a structured format.

**PHP/Python/Perl (Programming Language)**: The "P" represents the programming language used for server-side scripting. PHP is the most common choice, but Python and Perl are also options. These languages are used to process dynamic content, interact with the database, and generate HTML for the web browser.


**What makes a LAMP stack different from LEMP Stack** 

The difference between a LAMP Stack and a LEMP Stack is their web servers. A LAMP Stack makes use of an apache web server and a LEMP Stack makes use of a nginx web server.
Both web servers have their advantages, while apache is highly versatile, nginx has a very high performance, capable of load balancing, serve as a reverse proxy and is the most widely used web server.

### Importance of LEMP Stack

**Performance**: Nginx is known for its efficient handling of concurrent connections and low resource usage, making the LEMP stack well-suited for high-performance web applications.

**Scalability**: The LEMP stack is designed to be scalable, allowing developers to easily scale web applications to accommodate increased traffic and demand.

**Flexibility**: Developers have flexibility in choosing the programming language (PHP, Python, or Perl) based on their preferences and project requirements.

**Security**: The LEMP stack, when configured properly, can offer strong security features. Nginx, in particular, is praised for its security and ability to handle security threats effectively.

**Community Support**: The LEMP stack has a large and active community, providing extensive documentation, tutorials, and support. This community support is valuable for developers working with the stack.

### Target Audience

i. DevOps Engineers: DevOps professionals play a critical role in setting up and managing server environments. Understanding the LEMP Stack is essential for configuring web servers, optimizing performance, and ensuring robust security.

ii. System Administrators: System administrators responsible for server maintenance and security will find the LEMP Stack knowledge valuable. It enables them to set up and manage Linux-based web servers and databases effectively.

iii. Web Developers: Web developers looking to build dynamic websites and web applications can benefit from learning the LEMP Stack. They will gain the skills needed to create feature-rich, server-side applications.

iv. Database Administrators: Professionals tasked with database management will learn how to configure and maintain MySQL or MariaDB in the context of the LEMP Stack.

### Prerequisite

- An AWS account with an ubuntu EC2 instance
- Access to a terminal or virtual machine
- SSH. [click here](https://www.digitalocean.com/community/tutorials/how-to-set-up-ssh-keys-on-ubuntu-20-04) to learn more on ssh

### Project Goal

By the end of this project, learners will have the knowledge and ability to set up the whole LEMP stack. You will also have the skills to make impressive fast web applications that have a big effect in the always changing world of web development.

# Project Highlight
- LEMP Stack Implementation
- Introductin To LEMP Stack
  - What is LEMP Stack
  - Components Of A LEMP Stak
  - Importance of LEMP Stack
  - Target Audience
  - Prerequisite
  - Project Goal 

- Setting Up A LEMP Stack
  - Launch an Ubuntu Instance on AWS Console and ssh Into It Form Your Terminal
  - Installing Nginx
  - Installing Mysql
  - Installing PHP
  - Configuring Nginx Web server to Serves As A Virtual Host
  - Testing PHP with Nginx
  - Testing PHP and Mysql with Nginx (LEMP Stack)

- Real Life Scenarios With LEMP Stack

- Conclusion


# Setting Up A LEMP Stack
Let's get our hands on LEMP.
**Note**: Some of the steps are the same as that of LAMP Stack

#### Step 1: Launch an Ubuntu Instance on AWS Console and SSH Into From Your Terminal

We were able to launch an ubuntu instance in the previous project, refer to step 1 of [LAMP_Stack_Implementation](https://gitlab.com/Ridwan010/darey-pbl/-/blob/05422f458c6152e4f4407b94216fd01a855287ac/project6a.md) for a guide to launch an instance and ssh into it.

![EC2 Instance](Images/lemp/lemp-server.PNG)

#### Step 2: Installing Nginx
As usual, we need to always update package list when we launch a new instance

update package lists and apt repositories

    sudo apt update
Install nginx web server

    sudo apt install nginx
Allow firewall for nginx

    sudo ufw allow in "Nginx HTTP"
NOTE: The step above will allow firewall for nginx on port 80, to allow firewall for nginx on port 443 or any other port, "Nginx HTTPS and Nginx Full" need to be opened. Once it is enabled, it is important we allow firewall for ssh. ssh runs on port 22, if firewall is not allowed for ssh running on port 22, connection to the ubuntu instance via ssh might be permanently denied 

    sudo ufw allow 22
    
To check if nginx web server has been installed successfully

    sudo systemctl status nginx

![Nginx](Images/lemp/nginx-status.PNG)


To access your web server on your browser

    http://ubuntu_instance_public_ip_address

![Nginx](Images/lemp/nginx-default.PNG)


The Nginx default page above will displayed. 
You can check your ubuntu instance ip address from your aws console from the EC2 instance service management or input the command below

    curl http://icanhazip.com
or

    curl -4 icanhazip.com 


#### Step 3: Installing Mysql

In the previous, we were able to install apache web server successfully. In this step, mysql will be installed as a database to store data for our web application.
apt repositories has been updated in the previous step, mysql should be installed directly

    sudo apt install mysql-server

![mysql install](Images/lemp/install-mysql.PNG)

Check if mysql has been successfully installed

    sudo systemctl status mysql 

![mysql status](Images/lemp/mysql-status.PNG)

Let's log in to mysql server as the root user

    sudo mysql

We have been able to log into mysql server successfully, but our database is not yet secure, we need to run mysql security installation script, but before that we need to create a password for the root user, if not the root user might be unable to login to mysql after runnig the script.

Still logged in as the root user, let,s create a password

    ALTER USER 'root'@'localhost' IDENTIFIED WITH mysql_native_password BY 'MypassKey1.';

You can now exit mysql prompt

    mysql>exit
    
Now let's secure mysql installation 

    sudo mysql_secure_installation

Running the installation script above, will ask for a password validation from the root user. We need to validate it using the password we created inside mysql prompt earlier
```
Securing the MySQL server deployment.

Enter password for user root:
```
This will then ask you to validate password component. When MySQL prompts you to validate a password component, this component checks the strength of passwords and enforces a password policy. 
Choosing "No" allows you to set up a password policy that aligns with your organization's security requirements. It may involve configuring the password complexity rules, length, expiration policies, and other parameters according to your specific needs.It is recommended to select any other key for no
```
VALIDATE PASSWORD COMPONENT can be used to test passwords
and improve security. It checks the strength of password
and allows the users to set only those passwords which are
secure enough. Would you like to setup VALIDATE PASSWORD component?

Press y|Y for Yes, any other key for No:
```
Follow the image below for other optional steps

![Secure Installation](Images/lamp/mysql-installation.PNG)

Log into mysql as the root user

    sudo mysql -u root -p
The `-u` flag tells mysql which user is trying to login to the server and the `-p` flag show a prompt for passowrd on executing the command.
Log out of mysql

    mysql>exit

![Mysql login](Images/lemp/mysql-login.PNG)


#### Step 4: Installing PHP

So far, we have be able to install the first three technology stack of our LEMP stack. The last software of the LEMP stack which is PHP will be installed in this step.

The installation of php for nginx is different from apache because, the way it interacts with the web server differs. PHP for Apache typically uses the Apache module called mod_php. This module integrates PHP directly into the Apache web server, allowing it to handle PHP requests internally. For Nginx, PHP is typically run as a separate process using FastCGI (e.g., PHP-FPM) or as a reverse proxy. This means Nginx communicates with the PHP interpreter over a network socket or through a unix domain socket.

    sudo apt install php-fpm php-mysql

Check for the successful installation of php

    php -v

 ![PHP Version](Images/lemp/php-version.PNG)


#### Step 5: Configuring Nginx Web Server To Serve As A Virtual Host 
Create a directory called `darey.io` for our codes to be hosted at the location `/var/www/html`, `darey.io` can be named any name. The directory will contain the php codes which nginx will serve. The codes are not limited to php codes but also html, css, javascript e.t.c. . Nginx web server is smart enough to know this location and serve it with the help of its configuration file.

    sudo mkdir /var/www/html/darey.io
Create an simple html file which Nginx will serve

    sudo nano /var/www/html/darey.io/index.html
It should have the content below


    <h1>Welcome to Darey.io, Nginx works</h1>
darey.io is the directory created which will contain our php code
Assign ownership of the directory with the user

    sudo chown -R $USER:$USER /var/www/html/darey.io

Create a new server block configuration that will replace nginx default server block configuration at /etc/nginx/sites-available

    sudo nano /etc/nginx/sites-available/darey.io

Paste the content below in darey.io

    server {
    listen 80;
    server_name darey.io www.darey.io;
    root /var/www/html/darey.io;

    index index.html index.htm index.php;

    location / {
        try_files $uri $uri/ =404;
    }

    location ~ \.php$ {
        include snippets/fastcgi-php.conf;
        fastcgi_pass unix:/var/run/php/php7.4-fpm.sock;
     }

    location ~ /\.ht {
        deny all;
    }
    }

![Configuration File](Images/lemp/config-file.PNG)

Unlike apache, nginx only uses the server block configuration file in sites-enabled, sites-available only contains the different server block configuration.
To activate a server block, you create a symbolic link from the sites-enabled directory to the configuration file in the sites-available directory. This link tells Nginx to use that configuration file.

Creating a symbolic link 

    sudo ln -s /etc/nginx/sites-available/darey.io /etc/nginx/sites-enabled/

Unlinking the default server block configuration file from sites enables

    sudo rm /etc/nginx/sites-enabled/default
or

    sudo unlink /etc/nginx/sites-enabled/default

Reload nginx conf. file to make sure the file has no errors

    sudo nginx -t

![Nginx Test](Images/lemp/nginx-test.PNG)

Reload nginx

    sudo systemctl reload nginx

Check your web browser.

    http://ubuntu_instance_public_ip_address

![Nginx](Images/lemp/nginx-staticpage.PNG)


#### Step 6: Testing PHP with Nginx 

Now, let's engineer nginx to interact with php. We need to replace the index.html file in `/var/www/html/darey.io` with `index.php` with a simple php info.

    sudo nano /var/www/darey.io/html/index.php
Paste the code snippet below

    <?php
    phpinfo();
Save and close the file 
You can use ubuntu instance public ip address to access the php file served by nginx from your web browser 

![php](Images/lemp/php-info.PNG)

The default page above will be displayed.

#### Step 7: Testing PHP and Mysql with Nginx (LEMP Stack)

In the previous step, we were able to make nginx interact with php by engineering nginx to serve a php info page. In the step we will engineer our stack so they can work as a whole
The first to do is to create a database with a new user and database

Log into mysql as root user. Make sure you enter the password we created in step 3 in order to gain access to the database.

sudo mysql -u root -p


In the mysql prompt
Create a database called darey_io
```
CREATE DATABASE darey_io;
```

Create a new user called darey with password A different username or password can be used

CREATE USER 'darey'@'%' IDENTIFIED WITH mysql_native_password BY 'Ab123456789';


Grant 'darey' all permissions

GRANT ALL ON darey_io.* TO 'darey'@'%';


Log out of mysql

exit

![Root User](Images/lemp/root-login.PNG)


Log in to mysql as user darey
```
mysql -u darey -p 
```

Enter the passoword we created for darey

Create a new database called darey_io (This might throw an error that database already exist, but it's fine)
```
CREATE DATABASE darey_io;
```

Checkout the database created as the root user
```
SHOW DATABASES;
```

The output below will be printed
```
+--------------------+
| Database           |
+--------------------+
| darey_io           |
| information_schema |
+--------------------+
2 rows in set (0.000 sec)
```

Create a table called devops_list in the database created
```
CREATE TABLE darey_io.devops_list (
item_id INT AUTO_INCREMENT,
content VARCHAR(255),
PRIMARY KEY(item_id)
);
```

Add a few lines to the list
```
INSERT INTO darey_io.devops_list (content) VALUES ("Linux needed for devops");
INSERT INTO darey_io.devops_list (content) VALUES ("Nginx needed for devops");
INSERT INTO darey_io.devops_list (content) VALUES ("MySQL needed for devops");
INSERT INTO darey_io.devops_list (content) VALUES ("PHP needed for devops");
INSERT INTO darey_io.devops_list (content) VALUES ("LEMP needed for devops");
```
Check that the lines have been successfully inputed
```
SELECT * FROM darey_io.devops_list;
```

To output below will be shown
```
+---------+--------------------------+
| item_id | content                  |
+---------+--------------------------+
|       1 | Linux needed for devops  |
|       2 | Git needed for devops    |
|       3 | LAMP needed for devops   |
+---------+--------------------------+
3 rows in set (0.000 sec)
```

Exit mysql shell.

<table>
  <tr>
    <td><img src="Images/lemp/darey-login1.PNG" alt="Image 1"></td>
    <td><img src="Images/lemp/darey-login2.PNG" alt="Image 2"></td>
  </tr>
</table>

In the /var/www/html/darey.io we need to edit our index.php file
```
sudo nano /var/www/html/darey.io/index.php
```

Paste the code snippet below in the index.php file
```
<?php
$user = "darey";
$password = "Ab123456789";
$database = "darey_io";
$table = "devops_list";

try {
$db = new PDO("mysql:host=localhost;dbname=$database", $user, $password);
echo "<h2>Devops</h2><ol>"; 
foreach($db->query("SELECT content FROM $table") as $row) {
echo "<li>" . $row['content'] . "</li>";
}
echo "</ol>";
} catch (PDOException $e) {
print "Error!: " . $e->getMessage() . "<br/>";
die();
}
```
![PHP](Images/lamp/php-update.PNG)

Reload apache 

    sudo systemctl reload apache2

Check you web browser with your ubuntu instance ip address to see apache serving php with the contents in the database displayed.

![Devops List](Images/lemp/devops-list.PNG)


### Side Hustle Task

i. Launch an ubuntu instance and ssh into it.

ii. Set up a LEMP Stack

iii. Download a simple bootstrap code to host on your Stack. Use <wget https://github.com/startbootstrap/startbootstrap-agency/archive/gh-pages.zip>

iv. unzip the folder host it on your LEMP Stack 

***Hint:*** unzip the folder with the command "unzip" if not installed, installed unzip with 

	sudo apt install unzip

 Then move the whole folder you unziped to your project folder and update the server block configuration file.


# Real Life Scenarios With LEMP Stack

A situation whereby you work for an e-commerce company which is experiencing rapid growth and needs to scale its online store infrastructure to handle increased traffic and ensure high availability during peak shopping seasons.

As a DevOps engineers you can leverage the LEMP Stack to set up a scalable and high-performance e-commerce platform. You can use Nginx as a load balancer to distribute incoming traffic across multiple web server instances running PHP. MySQL databases are configured for replication to enhance redundancy and reliability. Additionally, they implement auto-scaling in the cloud to dynamically adjust server resources based on demand. This setup ensures seamless shopping experiences for customers, even during traffic spikes.

# Conclusion

In this project, we have been able to build a LEMP Stack,the LEMP Stack is a versatile and powerful technology stack that holds immense value for DevOps engineers and other technology professionals
To deepen your understanding and proficiency in the LEMP Stack, learners are encouraged to keep practicing and explore further, experiment with different configurations, and apply your knowledge to real-world projects.
