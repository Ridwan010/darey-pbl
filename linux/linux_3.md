## File Permissions and Access Rights
Understanding how to manage file permissions and ownership is crucial in Linux. This knowledge empowers you to control access to files and directories, ensuring the security and integrity of your system. Let's explore some essential commands and concepts related to file permissions and ownership.

In Linux, managing file permissions and ownership is vital for controlling who can access, modify, or execute files and directories. Understanding these concepts allows you to maintain the security and integrity of your system. Let's delve into the key commands and concepts related to file permissions and ownership.

## Numeric Representation of Permissions
In Linux, permissions are represented using numeric values. Each permission (read, write, and execute) is assigned a numeric value: read (4), write (2), and execute (1). These values are combined to represent the permissions for each user class.

A user class is represented by "-". Hence, each hyphen in this example represent each user class "---". The order is;
- The first `"-"`  is the "user"
- The second `"-"` is the "group"
- The third `"-"` is "others"

Lets go into the details a bit more. 
- The number 0 represents no permission (---). 
- The number 1 represents execute permission (--x). 
- The number 2 represents write permission (-w-). 
- The number 3 represents write and execute permissions (-wx). 
- The number 4 represents read permission (r--). 
- The number 5 represents read and execute permissions (r-x). 
- The number 6 represents read and write permissions (rw-).
- The number 7 represents read, write, and execute permissions (rwx).
  
For example, if a file has permissions rwxr-xr--, it means:

- The owner (user) has read, write, and execute permissions (7). `rwxr`
- The group has read and execute permissions (5). `r-x`
- Others have read-only permissions (4). `r--`


Adding more users to the same group in a Linux system is a common task that enhances collaboration and simplifies permissions management. Here's a step-by-step guide on how to add additional users to an existing group:

#### File Permission Commands
To manage file permissions and ownership, Linux provides several commands:

### chmod command
The `chmod` command allows you to modify file permissions. You can use both symbolic and numeric representations to assign permissions to the user, group, and others.

Lets see an example.

Create an emply file using the `touch` command

```
touch script.sh
```

Check the permission of the file 

```
ls -latr script.sh
-rw-r--r-- 1 dare staff 0 Jul  6 23:38 script.sh
```

### What do you think the permission of the above output represent?

Now lets update the permission so that all the user classes will have execute permission

```
 chmod +x script.sh
```

The above command uses the chmod command with the `+x` option to grant execute permission to the file `script.sh`. The `+x` option adds the execute permission to the existing permissions of the file. In this case, it allows the file script.sh to be executed as a script.

Now lets check what the file permissions look like

```
ls -latr script.sh
-rwxr-xr-x 1 dare staff 0 Jul  6 23:38 script.sh
```

The basic syntax for chmod follows this pattern:

```
chmod [option] [permission] [file_name]
```

Lets consider another example. Imagine the owner of a file is currently the only one with full permissions to `note.txt`. 

To allow group members and others to read, write, and execute the file, change it to the -rwxrwxrwx permission type, whose numeric value is 777:

```
chmod 777 note.txt
```

Check the output

```
ls -latr note.txt
-rwxrwxrwx 1 dare staff 0 Jul  6 23:53 note.txt
```

Now, notice the dash ("-") in the first position represents the file type and not a user class. It indicates that the entry is a regular file.

Here's a breakdown of the permission notation:

The first character represents the file type. Below is a list of all the file types you may ever come across:

- "-" indicates a regular file.
- "d" indicates a directory.
- "l" indicates a symbolic link.
- "c" indicates a character special file.
- "b" indicates a block special file
- "s" indicates a socket file.
- "p" indicates a named pipe.


##### chown command
The chown command allows you to change the ownership of files, directories, or symbolic links to a specified username or group. 

Here’s the basic format:

```
chown [option] owner[:group] file(s)
```

For example, lets assume there is a user on the server called **"john"**, a group on the server called **"developers"** and you want the owner of `filename.txt` changed from **"dare"** to **"john"**, and to also ensure that any user in the developer group has ownership of the file as well:

The command would look like below;

```
chown john:developer filename.txt
```

Check the output with `ls -latr` command on this file to then see the new changes.

## User, Group, and Others
In Linux, file permissions and ownership are categorized into three user classes: user, group, and others.

**User**: The user class represents the owner of the file or directory. As the owner, you have certain privileges and can control the permissions for yourself.

**Group**: The group class consists of users who belong to the same group as the file or directory. Group permissions define what members of that group can do with the file or directory.

**Others**: The others class encompasses all users who are neither the owner nor a member of the group. It specifies the permissions for any other user accessing the file or directory.

### Superuser Privilledges

It is often necessary to become the superuser to perform important tasks in linux, but as we know, we should not stay logged in as the superuser. In most linux distributions, there is a command that can give you temporary access to the superuser's privileges. This program is called sudo (short for super user) and can be used in those cases when you need to be the superuser for a small number of tasks. To use the superuser privilledes, simply type `sudo` before the command you will be invoking

To switch to the root user, simply run
```
sudo -i
```
You can type `exit` to leave the shell

![Root](image/root.PNG)


## User Management on Linux

As a DevOps engineer, you are also going to be doing systems administration which involves managing different users on the servers. You should know how to create a new user, or group, modify their permissions, update password and such similar tasks.
### Creating a User

To create a new user on Ubuntu Server, you can use the `adduser` command. Assuming the name of the user to be created is **joe**. Open the terminal and run the following command:

```
sudo adduser johndoe
```
running this command will prompt you to enter and confirm a password for the new user. You will also be asked to provide some additional information about the user, such as their full name and contact information. Once you provide the necessary details, the user account will be created, and a home directory will be automatically generated for the user.

The home directory represents a file system directory created in the name of the user. Such as `/home/johndoe`. This is where each user created on the server will store their respective data.

![Add User](image/adduser.PNG)

**Granting Administrative Privileges**

By default, newly created user accounts do not have administrative privileges. To grant administrative access to a user, you can add the user to the sudo group. Users in the sudo group can run commands with administrative privileges. To the **johndoe** user to the sudo group, run:

```
sudo usermod -aG sudo johndoe
```
- `usermod:` This is a command that modifies user account properties.
- -aG: These are flags used with the usermod command.
    - -a stands for "append" and is used to add the user to the specified group(s) without removing them from other groups they may already belong to.
    - -G stands for "supplementary groups" and is followed by a comma-separated list of groups. It specifies the groups to which the user should be added or modified. 
- In the given command, `-aG sudo` is used to add the user `johndoe` to the **sudo** group. 
- The sudo group is typically associated with administrative or superuser privileges. By adding `johndoe` to the `sudo` group, the user gains the ability to execute commands with elevated privileges.


**🛠️ Tasks for you:**
- Log out and log back in as the newly created user
- Navigate to the `/home/johndoe` directory to explore what has been created. **📌 Tip:** Use the `cd` command.

**Switching User Accounts**

To start using the system as another user, you will need to use the `su` command to switch. 

To switch to another user account, use the `su` command followed by the username. For example, to switch to the johndoe account, run:

```
su johndoe
```

You will be prompted to enter the password for the user. Once authenticated, you will switch to the user's environment.

### Modifying User Accounts
  
**Changing User Password**

To change the password for a user, use the `passwd` command followed by the username. For example, to change the password for **johndoe**, run:

```
sudo passwd johndoe
```

You will be prompted to enter and confirm the new password for the user.

**🛠️ Tasks for you:**
- Test the updated password by logging on to the server, using the newly updated password.

### Creating a Group 

To create a new group, use the  `groupadd` command. For example, to create a group named "developers," use:

```bash
sudo groupadd developers
```

### Adding Users to the Group

Use the `usermod` command to add users to the group. For instance, to add users "john" and "jane" to the "developers" group:

```bash
sudo usermod -aG developers johndoe

```

- The `-aG` options append the "developers" group to the users' existing group memberships.

### Verifying Group Memberships

To confirm the group memberships for a specific user, use the `id` command. For example, to check the group memberships for the user "johndoe":

```bash
id johnoe
```

This command displays information about the user "johndoe," including the groups they belong to, such as "developers."

![Add Group](image/addgroup.PNG)

### Deleting a User

To delete a user, run the command below
```
sudo userdel username
```

###  Ensuring Proper Group Permissions

Groups in Linux are often used to manage permissions for files and directories. Ensure that the relevant files or directories have the appropriate group ownership and permissions. For example, to grant the "developers" group ownership of a directory:

```bash
sudo chown :developers /path/to/directory
```

And to grant read and write permissions to the group:

```bash
sudo chmod g+rw /path/to/directory
```


#### **💡 Side Hustle Task 3 ⏱️:**

- Create a group on the server and name it `devops`
- Create 5 users `["mary", "mohammed", "ravi", "tunji", "sofia" ]`, and ensure each user belong to the devops group
- create a folder for each user in the `/home` directory. For example `/home/mary`
- Ensure that the group ownership of each created folder belongs to **"devops"**


