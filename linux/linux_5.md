
 ## SHELL SCRIPTING

With the thousands of commands available to the command line user, how can we remember them all? The answer is, we don't. The real power of the computer is its ability to do the work for us. To get it to do that, we use the power of the shell to automate things. We write shell scripts.

### What is Shell Scripting

Shell scripting is the process of writing and executing a series of instructions in a shell to automate tasks. A shell script is essentially a script or program written in a shell language, such as Bash, sh, zsh, or PowerShell ?

What Is a Shebang (#!/bin/bash) At the beginning of a script, is a shebang. It is a special notation used in Unix-like operating systems, to specify the interpreter that should be used to execute the script. In this case, #!/bin/bash specifically indicates that the Bash shell should be used to interpret and execute the script.

The #! (pronounced as  "hashbang"): This character sequence tells the operating system that what follows is the path to the interpreter that should be used to execute the script.

/bin/bash: This is the absolute path to the Bash shell executable. It tells the system to use the Bash interpreter located at /bin/bash to run the script.

When a script with a shebang line like #!/bin/bash, the operating system looks for the specified interpreter (in this case, Bash) and uses it to execute the script.

Without a shebang line, the system may not know how to interpret and execute the script, and you may need to explicitly specify the interpreter when running the script.
 
### Variables

Bash allows you to define and work with variables. Variables can store data of various types such as numbers, strings, and arrays. You can assign values to variables using the = operator, and access their values using the variable name preceded by a $ sign. Example: Assigning value to a variable:

name="John"

A variable usually takes a value, in other words, a variabe stores a value. A value can be a character, number, string or array a value is assigned to a variable.

From the example above, "John" was asigned to the variable "name". In the next example, we would see how the variable name saved a value

Example: Retrieving value from a variable:
```
echo $name
```

![Variables](image/variables.PNG)

echo is a command used to print a text, variables or values. In this example, echo is used to print a variable which stores a value .


### Control Flow
 Bash provides control flow statements like if-else, for loops, while loops, and case statements to control the flow of execution in your scripts. These statements allow you to make decisions, iterate over lists, and execute different commands based on conditions. Example: Using if-else to execute script based on a conditions

#!/bin/bash

read -p "Enter a number: " num

if [ $num -gt 0 ]; then echo "The number is positive." elif [ $num -lt 0 ]; then echo "The number is negative." else echo "The number is zero." fi The piece of code prompts you to type a number and prints a statement stating the number is positive or negative.

The read command in the script above takes input (a number) from the user The second line starting with an if statement tells that if the number inputed by the user is greater than 0, the third line should print "This number is positive" using the echo command The fourth line starting with an elif statement tells that if the number inputed by the user is less than 0, the fifth line should print "The number is negative" using the echo command The sixth line starting with an else statement tells that if the number inputed by the user is 0, the seventh line should print "The number is zero" using the echo command

Example: Iterating through a list using a for loop
```
#!/bin/bash

for (( i=1; i<=5; i++ )) do echo $i done
```


### Functions

A function is a named set of instructions that can be defined once and executed multiple times within a script. Functions provide modularity and help organize code by encapsulating a specific task or a series of commands. They make the script more readable, maintainable, and reusable.

Here's a basic structure of a function in shell scripting:

```
function_name() {
    # Commands or code block
    # ...
    # Return statement (optional)
    return
}
```
function_name: This is the name of the function, and it should follow the same naming rules as variable names.

Commands or code block: This is the set of instructions or commands that the function will execute when called.

Return statement (optional): The return statement is used to exit the function. You can also specify a value to be returned to the calling part of the script.

### Writing and Running A Simple 
```
#!/bin/bash
echo "Current directory: $PWD"
echo "creating a new directory ..."
mkdir darey_io 
echo "darey_io created"
echo "changing to new directory"
cd darey_io
echo "Current directory: $PWD"
echo "creating files"
touch bash1.txt
touch bash2.txt
echo "Files created"
echo "Files in the current directory:"
ls
echo "Moving back to previous directory"
cd ..
echo "Current directory: $PWD"
echo "Removing the new directory"
rm -rf darey_io
echo "Directory removed."
echo "Files in the current directory:"
ls
```
To run the script above follow the steps below

i. Create a file called script.sh (All bash files must end with `sh`)

ii. Open a text editor, start with  `#!/bin/bash` shebang.​

iii. Make theshell executable by running `sudo chmod +x script.sh`.​

iv. Execute with ./script.sh in the terminal.​


![Script](image/script.PNG)

The script above;

- Prints the current directory.

- Creates a directory called "darey_io."

- Changes to the "darey_io" directory.

- Prints the current directory (which should be inside "darey_io").

- Creates two empty files in the "darey_io" directory.

- Lists the files in the current directory (inside "darey_io").

- Moves back to the previous directory.

- Prints the current directory (which should be the original directory).

- Removes the "darey_io" directory and its contents.

- Lists the files in the current directory (after removing "darey_io").