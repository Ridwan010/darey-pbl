## LINUX FOUNDATION WITH SHELL SCRIPTING

## Introduction
In the dynamic landscape of technology, mastering the fundamentals is essential for anyone aspiring to excel in fields such as DevOps, Cloud Computing, Software Development, Cybersecurity, Data Analysis/Science, AI, and QA Testing. The project we're delving into is titled "Linux Foundation With Shell Scripting." This project is designed to equip learners with a solid foundation in Linux, a widely used operating system, coupled with the powerful skill of shell scripting. Understanding these core elements lays the groundwork for success in various tech-centric careers.


### What Is Linux and Shell Scripting ?
---
The project aims to provide a comprehensive understanding of Linux, a robust and open-source operating system, along with proficiency in Shell Scripting. Linux is the backbone of many server environments, cloud platforms, and embedded systems. Shell scripting, on the other hand, allows automation and customization of various tasks, offering a significant boost to efficiency for IT professionals.

### Linux Distributions 
---
Linux distributions, often referred to as distros, are complete operating systems built around the Linux kernel. These distros package the Linux kernel with a range of software, libraries, and tools to provide a functional computing environment. They offer different configurations, desktop environments, package managers, and software repositories. Some of the Linux distributions are; 

**Ubuntu**: Ubuntu is one of the most widely recognized Linux distributions, known for its user-friendliness and ease of installation. It's an excellent choice for those new to Linux, as well as for everyday desktop computing. Ubuntu also has server editions for web hosting and cloud deployments. 

**CentOS**: CentOS is favored in enterprise and server environments due to its stability and long-term support. It is essentially a free and open-source version of Red Hat Enterprise Linux (RHEL). System administrators often choose CentOS for its reliability and robustness. 

**Debian**: Debian is known for its commitment to free and open-source software principles. It provides a wide range of software packages and supports multiple hardware architectures. Debian is highly regarded for its package management system. 

**Fedora**: Fedora is a cutting-edge distribution that focuses on integrating the latest software and technologies. It's a great choice for those who want to experiment with new features and applications. Fedora also serves as a testing ground for Red Hat's Enterprise Linux products. 


### Advantages of Ubuntu Over Other Distros 
---
Ubuntu is a popular Linux distribution with a large user base, and it has several advantages over other Linux distributions, depending on your specific needs and preferences. Some of the advantages of Ubuntu include: 

**User-Friendly**: Ubuntu is known for its user-friendly interface, making it a great choice for users who are new to Linux. It offers a well-designed desktop environment (typically GNOME) and provides an easy-to-navigate interface. 

**Stability**: Ubuntu is based on Debian, which is known for its stability. It undergoes rigorous testing, and the LTS (Long Term Support) releases, in particular, provide a stable platform for both desktop and server use. 

**Cloud and Server Support**:** Ubuntu has a strong presence in the server and cloud computing space. Ubuntu Server is widely used for deploying web servers, cloud infrastructure, and containers. Ubuntu's server versions are well-suited for DevOps and cloud operations. 

**Large Community and Support**: Ubuntu has a vast and active user community, which means you can easily find help and resources online. There are numerous forums, documentation, and community-driven support options available.


### Importance of Linux
---
Linux is prevalent in the tech industry, powering a substantial portion of servers and embedded devices. Proficiency in Linux is not just a technical skill but a strategic asset, particularly for those involved in DevOps, Cloud Computing, and Cybersecurity. Similarly, shell scripting facilitates automation, enabling professionals to streamline repetitive tasks, making their workflow more efficient. Understanding both Linux and shell scripting is crucial for optimizing workflows, ensuring system security, and contributing to the seamless functioning of various technologies.


### Target Audience
---
This project is designed for individuals pursuing careers in DevOps, Cloud Computing, Software Development, Cybersecurity, Data Analysis/Science, AI, and QA Testing. It caters to those who are eager to enhance their technical skills and establish a strong foundation in Linux and shell scripting. Professionals seeking to expand their skill set or students preparing for roles in technology-related fields will find this project beneficial.

### Project Prerequisites
---
To make the most of this project, participants should have a basic understanding of computer systems and familiarity with fundamental programming concepts. Prior exposure to the Linux environment and basic command-line operations would be advantageous.
Also learners should have their working environment setup. Overall, a willingness to learn and a passion for technology are the key prerequisites for a successful engagement with the Linux Foundation With Shell Scripting project.

### Project Goals
---
The Linux Foundation With Shell Scripting project is designed with specific learning objectives to empower participants with essential skills and knowledge. By the end of this project, readers should aim to achieve the following goals:

**Mastery of Linux Fundamentals**: Gain a comprehensive understanding of the Linux operating system, covering key concepts, file systems, user management, and permissions.
Acquire proficiency in using the Linux command line to navigate the system, manage files, and execute essential commands.

**Shell Scripting Proficiency**: Develop a solid foundation in shell scripting languages, with a focus on Bash scripting.
Learn to create, modify, and execute shell scripts to automate routine tasks and enhance workflow efficiency.

**Application of Linux in DevOps and Cloud Environments**: Understand how Linux is integral to DevOps practices, including continuous integration, deployment, and configuration management.
Explore the role of Linux in cloud computing environments and how it forms the basis for various cloud services.

**Security Best Practices**: Grasp fundamental Linux security principles and practices to safeguard systems and data.
Implement security measures through user management, permissions, and awareness of potential vulnerabilities.

**Preparation for Advanced Concepts**: Lay the groundwork for more advanced topics in technology fields, such as containerization (e.g., Docker) and orchestration (e.g., Kubernetes), which often build upon a solid Linux foundation.

**Real-world Application and Problem-Solving**: Apply acquired knowledge to real-world scenarios and problem-solving, fostering practical skills applicable in professional settings.
Engage in hands-on exercises and projects to reinforce theoretical concepts.

**Career Readiness**: Enhance career readiness by gaining skills relevant to various technology-driven professions, including DevOps, Cloud Computing, Software Development, Cybersecurity, Data Analysis/Science, AI, and QA Testing.

By achieving these goals, learners will be well-equipped to navigate the intricate world of Linux, shell scripting, and related technologies, positioning themselves for success in their chosen tech careers.

## Project Highlight
---
**Package managers**

## Installation and Initial Setup
---
In this section, we will create a server in the cloud, and gain access to it from our local environment(powershell, mac terminal, virtualbox or termius). We will use [AWS](https://aws.amazon.com/), a public cloud provider to create the server. For now, do not worry about trying to learn AWS, or any other cloud provider because there is plenty of projects ahead that focuses on that.
Right now, all we need to know is that AWS can provide us with a free virtual server called EC2 [Elastic Compute Cloud](https://aws.amazon.com/ec2/features/) for our needs.

Let us create an EC2 instance (an ubuntu linux of a virtual server to be presice) is only a matter of a few clicks.
You can either Watch the videos below to get yourself set up.

[AWS account setup and Provisioning an Ubuntu Server](https://youtu.be/xxKuB9kJoYM?si=eo8ySDNmoi3_9JZu)

[Connecting to your EC2 Instance](https://youtu.be/TxT6PNJts-s?si=OWV8I_y3IV2R-EMe)


Or follow the guideline below.

**i. Register a new AWS account following [this instruction](https://repost.aws/knowledge-center/create-and-activate-aws-account)**.

**ii. Sign in to your AWS account**

**iii. On the top right select services and search for Elastic Cloud Compute (EC2).**

**iV. From the menu on the left side, select instances**.

**v. Select launch instance on the top right side**.

**vi. Follow the image below to finish lauching an instance**.

![AWS EC2](image/ec2-gif.gif)


**Important**: Be sure to launch an Ubuntu 20.04LTS instance. Also keep your `key.pem` key safely anyone with the key can have access to your instance.

#### [SSH](#ssh)

Now let's connect to the linux virtual server we launched from AWS. In order to do this, we need to use an SSH agent

SSH into your ubuntu instance

For windows, open command prompt or powershell

```
ssh -i "path/to/key.pem" ubuntu@public_ip_address
```

For mac and linux, open a terminal
```
ssh -i "path/to/key.pem" ubuntu@public_ip_address
```
![SSH](image/connect-instance.PNG)

**Note**: path/to/key.pem should be replaced with the actual path to key.pem file that was downloaded on our local computer when creating the instance and public_ip_address should be replaced the actual ubuntu instance public ip address. We can get our public ip address here

![Public IP](image/publicip.PNG)

## Recommended
### SSH Agent

An SSH agent is a program that holds private keys used for public key authentication (a method of logging into a remote server securely without a password). It helps manage your SSH keys by storing them in memory, eliminating the need to repeatedly enter the passphrase for your private key each time you connect to a remote server.

Here's how an SSH agent typically works:

**Generate SSH Key Pair**: You first generate an SSH key pair: a private key (usually kept on your local machine) and a corresponding public key (usually added to the ~/.ssh/authorized_keys file on the remote server).

**Passphrase Protection**: You may choose to add a passphrase to your private key for an additional layer of security. This passphrase is used to encrypt the private key.

**SSH Agent**: Instead of manually entering the passphrase each time you connect to a server, an SSH agent can be used to store the decrypted private key in memory during your session. This way, you only need to enter the passphrase once when you start the agent.

**Agent Forwarding**: Another useful feature of SSH agents is agent forwarding. If you are connecting from one server to another, agent forwarding allows you to use the keys from your local machine on the remote server, eliminating the need to copy private keys between machines.

You can learn more about [SSH](https://www.ssh.com/academy/ssh)

### Package Managers

Package managers in Linux are tools that automate the process of installing, updating, configuring, and removing software packages on a Linux distribution. They simplify the management of software by handling dependencies, versioning, and installation procedures. There are several package managers used in various Linux distributions.

**Commonly Used Package Managers**
**APT (Advanced Package Tool)**: Used by Debian-based distributions such as Debian itself, Ubuntu, and derivatives. Commands include apt-get and apt.

**YUM (Yellowdog Updater Modified)**: Originally used by Red Hat and CentOS, YUM is now largely replaced by dnf in modern Red Hat-based distributions. It simplifies package management by resolving dependencies.

**DNF (Dandified YUM)**: Used in modern versions of Red Hat-based distributions as a replacement for YUM. It provides improved performance and resolves some of the limitations of the older YUM tool.

You can learn more about package managers [Package Managers](https://www.linode.com/docs/guides/linux-package-management-overview/)


## Installing, Updating and Removing Software
The previous step talked about package manager and what it means. The `apt` package manager will be put into use in this step since we launched an ubuntu linux virtual server earlier.

We will dive into the practical aspects of managing software packages on a Linux system. This fundamental skill is crucial for maintaining a well-functioning and up-to-date environment. The step will cover installing, updating, and removing software packages using package managers commonly found in Linux distributions.


 It is recommended to read on package managers if you skipped the step to understand why we are using `apt`

Still connected to our linux server if not run this [step](#ssh) again, let run the command below

For now you do not have to worry about `sudo`, it will explained better in the later part of this course

1. **Updating Package Lists**
Before installing new software or updating existing packages, it's important to refresh the package lists.

```
sudo apt update      # For Debian/Ubuntu-based systems

sudo yum update      # For Red Hat/Fedora-based systems
```

![Linux Update](image/apt-update.PNG)


2. **Installing Software Packages**
Explore the process of installing new software using your system's package manager.

The tree command is not a standard built-in command in most Linux distributions, but it's a commonly used and widely available command that you can install on ubuntu distro. So let's install it with the command below.

sudo apt install tree     # Debian/Ubuntu

sudo yum install tree      # Red Hat/Fedora

![Install Package](image/apt-install.PNG)

3. **Verifying Installed Packages**
To confirm that the desired packages are installed. For example the `tree` command we previously installed can be verified with the command below


dpkg -l                # Debian/Ubuntu

rpm -qa                 # Red Hat/Fedora

4. **Updating Installed Packages**
Keep your system up-to-date by updating installed packages.

```
sudo apt upgrade      # Debian/Ubuntu

sudo yum update       # Red Hat/Fedora
```

5. **Removing Software Package**
To remove the `tree` package we installed earlier, run the  below command

```
sudo apt remove tree       # Debian/Ubuntu

sudo yum remove tree       # Red Hat/Fedora
```

![Remove Package](image/apt-remove.PNG)

6. **Cleaning Package Cache**
Free up disk space by cleaning the package manager's cache.

```
sudo apt clean         # Debian/Ubuntu

sudo yum clean all     # Red Hat/Fedora
```

In the next step, we would be engaging in hannds on project that involve using commands on a Linux system. Working with Linux commands typically includes tasks such as navigating the file system, managing files and directories and manipulating files.