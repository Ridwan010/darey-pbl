TechOps Essential - Linux Capstone Project: My Linux Odyssey

## Table of Contents

1. [Tips for Linux Hosting](#tips-for-linux-hosting)
2. [Tips for Success](#tips-for-success)
3. [Submission](#submission)
4. [Step 1: Set Up Your Linux Environment](#step-1-set-up-your-linux-environment)
5. [Step 2: Mastering Basic Linux Commands](#step-2-mastering-basic-linux-commands)
6. [Step 3: Unlocking Advanced Linux Commands](#step-3-unlocking-advanced-linux-commands)
7. [Step 4: Becoming a Linux Text Editing Ninja](#step-4-becoming-a-linux-text-editing-ninja)
8. [Step 5: Scripting Your Linux Adventures](#step-5-scripting-your-linux-adventures)

### Tips for Linux Hosting

- [Linux Users and Permissions](#linux-users-and-permissions)
- [Software Management](#software-management)
- [Backup and Recovery](#backup-and-recovery)

### Tips for Success

- [Effective Communication](#effective-communication)
- [Version Control](#version-control)
- [Documentation Excellence](#documentation-excellence)
- [Collaborative Spirit](#collaborative-spirit)
- [Enjoy the Learning Journey](#enjoy-the-learning-journey)

### Submission

- [Submission Guidelines](#submission-guidelines)

---

### Step 1: Set Up Your Linux Environment

#### 1.1 Install Linux

Ensure that you have a Linux distribution installed on your machine. If not, launch a linux virtual machine from you cloud provider e.g., Ubuntu, Fedora (Ubuntu is recommended)

#### 1.2 SSH Configuration (Optional)

Connect to your machine using SSH 

```bash
# Use the key generated to connect to the linux instance

ssh -i "path/to/key" ubuntu@public-ip-address
```

#### 1.3 User Setup

Create a new user account with sudo privileges. Avoid using the root account for daily tasks.

```bash
# Create a new user
sudo adduser yourusername

# Add the new user to the sudo group
sudo usermod -aG sudo yourusername
```



### Step 2: Mastering Basic Linux Commands

#### 2.1 Navigation and File Operations

Practice basic commands like `ls`, `cd`, `cp`, `mv`, and `rm`. Create a file structure for your project.

```bash
# Navigate to your project directory
cd /path/to/your/project

# Create directories and files
mkdir chapter-1
touch chapter-1/chapter-1.md
```

#### 2.2 Package Management

Install, update, and remove software packages using package managers like `apt` or `yum`.

```bash
# Install a software package
sudo apt install software-package

# Update all installed packages
sudo apt update && sudo apt upgrade

# Remove a software package
sudo apt remove software-package
```

### Step 3: Unlocking Advanced Linux Commands

#### 3.1 User Permissions

Understand and apply file permissions using commands like `chmod` and `chown`.

```bash
# Change file permissions
chmod 755 filename

# Change file ownership
chown newowner:newgroup filename
```

#### 3.2 Access Control Lists (ACL)

Explore and apply Access Control Lists for fine-grained control over file permissions.

```bash
# View ACL of a file
getfacl filename

# Add an ACL entry
setfacl -m u:username:rw filename
```

### Step 4: Becoming a Linux Text Editing Ninja

#### 4.1 Vim

Learn the basics of Vim for efficient text editing.

```bash
# Open a file in Vim
vim filename

# Basic editing commands (i: insert, Esc: command mode, :wq: save and quit)
```

#### 4.2 Nano

Explore the user-friendly Nano text editor.

```bash
# Open a file in Nano
nano filename

# Basic commands (Ctrl + O: save, Ctrl + X: exit)
```

### Step 5: Scripting Your Linux Adventures

#### 5.1 Shell Scripting

Create shell scripts to automate tasks. Cover variables, control flows, and functions.

```bash
# Example script: chapter-1-script.sh
#!/bin/bash

# Variables
name="YourName"

# Control Flow
if [ "$name" == "YourName" ]; then
  echo "Hello, $name!"
else
  echo "You're not the expected user."
fi

# Function
greet() {
  echo "Greetings from a function!"
}

# Call the function
greet
```

---

#### Tips for Linux Hosting

#### Linux Users and Permissions

- Always follow the principle of least privilege when assigning permissions.
- Regularly review and update user permissions to adhere to the principle of need-to-know.

#### Software Management

- Use package managers to ensure software consistency across different environments.
- Document software dependencies and versions to facilitate future installations.

#### Backup and Recovery

- Implement regular backups of critical data and configurations.
- Test your backup and recovery processes to ensure they work seamlessly.

---

#### Tips for Success

#### Effective Communication

- Use clear and concise language in commit messages and documentation.
- Foster an open and collaborative communication environment within your team.

#### Version Control

- Make use of version control systems like Git for tracking changes and collaboration.
- Commit frequently with meaningful messages to provide a detailed history of your project.

#### Documentation Excellence

- Document every step of your Linux adventure, from setup to advanced commands.
- Prioritize readability and clarity in your documentation to aid future reference.

#### Collaborative Spirit

- Embrace collaboration by seeking and providing constructive feedback.
- Respect the work of your peers and maintain a positive collaborative spirit.

#### Enjoy the Learning Journey

- Linux administration can be challenging but rewarding; savor the learning process.
- Celebrate achievements, and don't hesitate to seek help or explore new ideas.

---

#### Submission Guidelines

- Submit a link to your Git repository containing the project.
- Include a README.md file with detailed instructions on setting up the Linux environment and navigating the project.

---

Embark on your Linux Odyssey, and may the commands be ever in your favor!