**Course Title: Linux For TechOps**

**Course Overview:**
This course is tailored for beginners, providing a gentle introduction to Linux. Through live sessions and hands-on project, learners will build a solid foundation in Linux basics, system navigation, and essential command-line skills.

**Course Curriculum**
Module 1: Introduction To Linux

Module 2: Basic Linux Commands

Module 3: Advanced Linux Commands

Module 4: Working With Linux Text Editors



**Module 1: Introduction To Linux**

**Lesson 1: Meet Linux**
- What is Linux and Why Use It?
- Overview of Linux Distributions
- Virtual Machines 
- SSH Keys

**Lesson 2: Command-Line Basics**
- Introduction to the Command Line
- Linux File System Heirarchy


**Lesson 3: Installing Linux**
- Understanding AWS EC2 Service
- Creating A Linux Virtual Server
- Basic Configuration After Installation





**Module 2: Basic Linux Commands**

**Lesson 4: Working With Files And Directories**
- Creating Empty Files
- Creating Directories
- 

**Lesson 5: Linux Navigation**
- Changing Directories
- Printing Working Directories
- Changing To Parent Directories
- Absolute Path

**Lesson 6: Looking Around**
- Concatenate and Dispaly
- Listing Files and Directories
- Long Format Listing
- Showing Hidden Files

**Lesson 7: Manipulating Files**
- Copying Files and Directories
- Moving Files and Diretories
- Removing Files
- Removing Directories
- Forcefully Removing Directories


**Module 3: Advanced Linux Commands**

**Lesson 8: File Permissions**
- Modifying File Access Rights
- Temporarily Become The Superuser
- Changing Ownership of A File
- Changing Group Ownership of a File

**Lesson 9: Pacakge Management**
- Introduction To Package Managers 
- Using Package Manager For Updating And Installing Softwares



**Module 4: Linux Text Editors**

**Lesson 10: Working With VIM**
- Introduction to vim
- Basic VIM Commands for Editing and Navigation
- Practical Exercises in VIM

**Lesson 11: Working With NANO**
- Introduction to nano
- Basic NANO Commands for Editing and Navigation
- Hands-On Practice with NANO

**Lesson 12: Working With EMACS**
- Introduction to EMACS
- EMACS Commands for Text Editing and Manipulation
- Interactive Exercises with EMACS

