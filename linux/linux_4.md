## VIM Text Editor

The Linux Vim text editor, short for "Vi Improved," is a powerful and versatile text editing tool deeply ingrained in the Unix and Linux ecosystems. Vim builds upon the foundation of the original Vi editor, offering an extensive set of features, modes, and commands that empower users to manipulate text efficiently. While Vim has a steeper learning curve compared to simpler editors like Nano, its capabilities make it a favourite among tech professionals and anyone working extensively with text files.

### Working With VIM Editor

Let's get our hand on vim

#### 1. Open a New File:
   - Open a new file named "exercise.txt" using the following command:
     ```bash
     vim exercise.txt
     ```
The command above creates a `exercise.txt` even if it doesn't exist

#### 2. Enter Insert Mode:
   - Press `i` to enter Insert mode.
   - Type the following text into the file:

     ```
     Hello, this is a Vim hands-on project.
     Welcome to darey.io.
     ```

2. Moving Around: Navigate through the text using the arrow keys or h (left), j (down), k (up), and l (right).

3. Deleting a Character:In Normal Mode, position the cursor on a character you want to delete and press x.

4. Deleting a Line: In Normal Mode, place the cursor on a line, and type dd to delete the entire line.

5. Undoing Changes: Make a change (add or delete text) in Insert or Normal Mode, then press Esc to enter Normal Mode and press u to undo the last change.

6. Search and Replace: In Normal Mode, type :/search/replace/g to search for the word "search" and replace it with "replace" globally.

7. Exiting Insert Mode: Press Esc to exit Insert Mode and return to Normal Mode.

8. Saving Changes: In Normal Mode, type :w and press Enter to save your changes.

9. Saving and Quitting: In Normal Mode, type :wq and press Enter to save and quit the file.

10. Quitting Without Saving: In Normal Mode, type :q! and press Enter to quit without saving changes.


## Nano Text Editor

Among Linux text editors, Nano stands out as a user-friendly and straightforward tool, making it an excellent choice for users who are new to the command line or those who prefer a more intuitive editing experience. Nano serves as a versatile and lightweight text editor, ideal for performing quick edits, writing scripts, or making configuration changes directly from the command line. Its intuitive command set simplifies text manipulation tasks, allowing users to navigate through files, insert or delete text, and save changes effortlessly. Nano's ease of use extends to its keyboard shortcuts, making it accessible even to those unfamiliar with intricate command sequences. With Nano, users can focus on the content of their text files without the distraction of a complex interface, making it a go-to choice for a wide range of users, from beginners to experienced Linux enthusiasts.

### Working With Nano Editor

#### 1. **Opening a File:**
   - Open a new file named "nano_project.txt" using the following command:
     ```bash
     nano nano_file.txt
     ```
   - You'll enter the Nano editor interface.

#### 2. **Entering and Editing Text:**
   - Type a few lines of text into the file. Nano has a simple interface, and you can start typing immediately.

#### 3. **Saving Changes:**
   - Save your changes by pressing `Ctrl` + `O`. Nano will prompt you to confirm the filename; press `Enter` to confirm.

#### 4. **Exiting Nano:**
   - Exit Nano by pressing `Ctrl` + `X`. If you have unsaved changes, Nano will prompt you to save before exiting.

#### 5. **Opening an Existing File:**
   - Open an existing file (if available) using the following command:
     ```bash
     nano existing_file.txt
     ```
   - Navigate through the file using arrow keys.

#### 6. **Search and Replace:**
   - Use `Ctrl` + `W` to initiate a search. Enter a word to search for, and Nano will highlight the first occurrence.
   - Press `Alt` + `R` to activate the replace feature. Replace the searched word with a new one.

#### 7. **Cut, Copy, and Paste:**
   - Cut a line by pressing `Ctrl` + `K`.
   - Copy a line by pressing `Alt` + `A`.
   - Paste the copied or cut text using `Ctrl` + `U`.

#### 8. **Navigating Through Text:**
   - Utilize Nano's easy navigation features to move around the file quickly. Try `Ctrl` + `B` for backward and `Ctrl` + `F` for forward.

#### 9. **Line Number Display:**
   - Toggle line numbers on and off by pressing `Alt` + `N`.
