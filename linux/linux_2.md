## LINUX COMMANDS

In this section, we delve into fundamental Linux commands that empower users to navigate, inspect, and manipulate files and directories. Understanding these commands is crucial for effective system management and day-to-day tasks in a Linux environment.

Sit tight while we dive in and explore the power of Linux commands for working with files and directories.


## Working With Files and Directory

**touch command**

To create or update files in the Linux file system, the touch command is our tool of choice. With touch, we can generate an empty file or update the access and modification times of an existing file.

The basic usage involves typing touch followed by the desired filename. For instance, to create a new file named "example.txt," the command would be touch example.txt.

Additionally, touch proves useful for altering the timestamps of files. By running touch on an existing file, it instantly updates the access and modification times to the current moment.

Let's delve into an example

```
touch file_1.txt
```

![Touch](image/touch.PNG)

In this case, we've employed touch to create file "file_1.txt." The simplicity and versatility of the touch command make it an essential component of creating file in the Linux command line environment. 

**echo command**

 The echo command in Linux is a versatile and fundamental tool for displaying text on the command line. Its primary function is to output text or variables to the terminal, making it a crucial component in shell scripts and command-line interactions.

To use echo, simply type the command followed by the text you want to display. For example:

```
echo "Hello, Linux World!"
```
This command outputs the text "Hello, Linux World!" to the terminal.

![Echo](image/echo1.PNG)

echo is not limited to fixed text; it can also handle variables and special characters. Here's an example:

```
name="Darey"
echo "Hello, $name! Today is $(date)"
```
![Echo](image/echo2.PNG)

In this example, echo prints a greeting that includes the value of the variable name and the current date.

Furthermore, echo can be employed for tasks beyond simple text output. It is frequently used in shell scripting to write or append content to files. For instance:

```
echo "Howdy, i am learning linux" >> file_2.txt
```

![Echo](image/echo3.PNG)

This command appends the text "Howdy, i am learning linux" to the file named file_2.txt.



**mkdir (Make Directory) command**

The `mkdir` command in Linux holds the key to creating directories, providing users with a straightforward means of organizing and structuring their file system.

To initiate the creation of a new directory, simply type `mkdir` followed by the desired directory name. For example:

```
mkdir darey
```
![Make Directory](image/mkdir1.PNG)

This command establishes a new directory named "darey" within the current working directory.

`mkdir` is not solely limited to creating a single directory; it accommodates the simultaneous creation of multiple directories. By specifying multiple directory names, separated by spaces, the command generates them all at once:

```
mkdir dir1 dir2 dir3
```

![Make Diretory](image/mkdir2.PNG)


This command results in the creation of three separate directories: "dir1," "dir2," and "dir3."

Additionally, `mkdir` supports the creation of parent directories along with their subdirectories in a single command. If a directory's path includes nonexistent parent directories, `mkdir` can create the entire directory tree:

```
mkdir -p parent/child/grandchild
```

![Make Directory](image/mkdir3.PNG)

Passing `parent/child/grandchild` to mkdir creates a directory named parent and creates another directory in called `child` inside the parent directory and creates another directory `grandchild` inside the child directory

The `-p` option ensures that both the parent and child directories, along with any intermediate directories, are created as needed.

Not to worry, in the next steps we will see and move within these directories



## Navigating Linux


**pwd (Print Working Directory) command**

In the linux command line interface, visualizing the file system structure can be likened to navigating through a maze. Imagine yourself standing within this maze, where each directory forms a pathway with files and connections to its parent and subdirectories.

The directory in which you currently stand is referred to as the working directory. To unveil the identity of this directory, the `pwd` command comes to your aid.

Upon initially logging into your Linux system, your starting point is the working directory, set to your home directory. Typically, the home directory adopts the format /home/user_name, although the username usually depend on the type of linux distributio you are using. We can see in the image below that the username is `ubuntu`

![Print Working Directory](image/pwd.PNG)

To list the files in the working directory, we use the `ls` command. Not to worry, come back to ls in the next lesson. There are a lot of fun things you can do with it, but we have to talk about pathnames and directories a bit first.

**cd (Change Directory) command** 

To alter our current position within the maze of directories, we employ the cd command. This entails entering cd followed by the pathname leading to the target working directory. Pathnames serve as the directional route along the branches of the directory tree.

Let's recall we created a directory earlier named `darey`. In order move into this directory run the command below

```
cd darey
```

![Change Directory](image/cd1.PNG)

We can see a change in the image above, `~/darey`show we have now working inside the `darey` folder. Running the `pwd command` also confirms our present working directory


**cd .. (Change to Parent Directory)**: This command moves you up one level in the directory hierarchy, effectively taking you to the parent directory of your current location.

You might be wondering now that we are inside a new directory, how do we move out of it or move back to our previous working directory, this is where the `cd ..` command comes in

![Change to Parent Directory](image/cd2.PNG)


We can see that after changing to our previous working directory, we ran the `pwd` command to confirm if we were able to successfully move back.

**cd /path/to/folder (Absolute Path)**: An absolute pathname commences from the root directory and systematically traces the branches of the tree until reaching the intended directory or file. For instance, consider a directory on your system housing most installed programs. Its absolute pathname is /usr/bin. This signifies that starting from the root directory (indicated by the leading slash in the pathname), there is a directory named "usr" containing a directory named "bin."

By utilizing absolute pathnames, we can precisely navigate through the directory structure, moving from the root downward along the defined path. We can use an absolute path to directly navigate to a specific directory, regardless of your current location. 

Let's recall we create a directory structure `parent/child/grandchild` in the previous step. Now to move into the grandchild folder, we can simply invoke the command below.

```
cd parent/child/grandchild
```

![Absolute Path](image/cd3.PNG)

In order to move to back to the parent directory we can use `cd ..` and likewise for the parent directory.




## Looking Around

 **ls (List) command**

  The `ls` command stands as a fundamental tool in the Linux command line toolkit, facilitating the exploration and enumeration of files and directories within a specified location.

To initiate a basic directory listing, enter `ls` followed by the path of the target directory.

In the previous steps we have me creating files and directories and moving around it in this location `/home/ubuntu` let us simply run the `cd` command to move back to the home directory

```
cd
```
Then we can now run `ls` to all the files and directories we have create

```
ls
```

![List](image/list1.PNG)


`ls` command comes with various options to tailor the output according to specific requirements. For instance:

- To list all files, including hidden ones, use the `-a` option:
  ```
  ls -a
  ```

- To display detailed information, such as file permissions, ownership, and modification times, use the `-l` option:
  ```
  ls -l
  ```

- For a combination of both, you can combine options:
  ```
  ls -la
  ```

Furthermore, `ls` supports wildcard characters to match specific patterns. For instance:

- To list all text files in a directory, you can use:
  ```
  ls *.txt
  ```

- To list all files and directories starting with the letter "a," you can use:
  ```bash
  ls a*
  ```
The `ls` command is a versatile and indispensable tool for inspecting the contents of directories. Whether seeking a simple list or detailed information, `ls` provides the flexibility and clarity needed for efficient file system navigation in the Linux environment.


**cat (Concatenate and Display) command**

 The `cat` command in Linux serves as a versatile utility for displaying the contents of files, creating new files, and concatenating multiple files. Its name, derived from "concatenate," reflects its original purpose, but it has evolved into a tool with a broader set of functionalities.

**Viewing File Contents:** Let's create a non empty file using `echo`command

```
echo "Hi, welcome to darey.io" >> f1.txt
```

The primary use of `cat` is to display the contents of a file on the terminal.

```
cat f1.txt
```

![Concatenate](image/cat1.PNG)

We can see the contents of "f1.txt" is displayed

**Creating New Files**: Like the `touch` command `cat` can be used to create a new file or overwrite an existing one. For instance, to create a file named "f2.txt" with specific content:

```
cat > f2.txt
This is the content of the new file.
Press Ctrl+D to save.
```

![Concatenate](image/cat2.PNG)


**Concatenating Files**: `cat` can concatenate the contents of multiple files. For example:

```
cat f1.txt f2.txt > combined.txt
```

This command combines the contents of "f1.txt" and "f2.txt" into a new file named "combined.txt."

![Concatenate](image/cat3.PNG)

**Displaying Line Numbers**: To display line numbers alongside the content, use the `-n` option:

```
cat -n filename.txt
```

This command numbers each line of "filename.txt" in the output.

**Appending to a File**: To append the content of one file to another, use the `>>` operator:

```
cat file1.txt >> file2.txt
```

This appends the content of "file1.txt" to the end of "file2.txt."



## Manipulating Files and Directories

**cp (Copy) command**

The `cp` command in Linux is a powerful tool for copying files and directories. It provides users with the capability to duplicate files and directories, whether within the same location or to a different destination.

Let us recall we created a files and directory in this [step](working with files and directory). Now let's change our working directory to `/home/ubuntu` where we have those files and directories

To copy a `file_1.txt` into `darey` , use the following syntax:

```
cp file_1.txt darey
```
We can change our directory with into darey `cd` command and verify this with `ls` command

![Copy](image/copy1.PNG)

This command duplicates "file_1.txt" to the specified "darey" directory. If you want to keep the same filename in the destination, you omit the destination filename.

**Copying Directories:** In the earlier steps, we created this directory structure `parent/child/grandchild` and also created some content inside it. 
Let us copy the entire directory and its contents to darey directory with the use `-r` (recursive) option with cp

```
cp -r parent darey
```

This recursively copies all directories and subdirectories from "parent" to "darey."

![Copy](image/copy2.PNG)


**Copying Multiple Files:** To copy multiple files to a directory, you can specify them all in the source:

```
cp file1.txt file2.txt file3.txt destination/
```

This command copies "file1.txt," "file2.txt," and "file3.txt" to the specified "destination" directory.


**mv (move) command**


The mv command in Linux is a powerful utility for moving or renaming files and directories. It allows users to relocate items within the same directory or to completely different locations in the file system.
While the `cp` command duplicate before copying a file, the `mv` command move the file entirely without a duplicate

**Moving Files**: To move a file to a different directory, use the following syntax:

```
mv file1.txt parent
```

![Move](image/move1.PNG)


This command relocates "file_1.txt" to the specified "parent" directory. If you want to rename the file during the move, you can specify a different filename in the destination.

**Moving Directories**: For moving entire directories and their contents, use the -r (recursive) option:
```
mv -r sourcedirectory/ destination/
```

This recursively moves all files and subdirectories from "sourcedirectory" to "destination."

**Renaming Files**: To rename a file without changing its location, provide the new filename in the destination:
```
mv oldname.txt newname.txt
```

This command effectively renames "oldname.txt" to "newname.txt" in the same directory.


**Moving Multiple Files**: To move multiple files to a directory, you can specify them all in the source:
```
mv file1.txt file2.txt file3.txt destination/
```

This command moves "file1.txt," "file2.txt," and "file3.txt" to the specified "destination" directory.


**rm (remove file) command**

The rm command is used to delete files within a directory.

⚠️ Caution:
This is a very dangerous command as it deletes the files completely. So must be used with care.
To remove a single file or directory:

```
rm filename
```

To remove multiple files, enter the following command:

rm filename1 filename2 filename3


Here are some acceptable options you can add:
```
-i prompts system confirmation before deleting a file. (Denotes "interractive")
-f allows the system to remove without a confirmation. (Denotes "force")
-r deletes files and directories recursively.
```
**rmdir command**

The rmdir command in Linux is specifically designed for removing empty directories. It provides a straightforward way to clean up your file system by deleting directories that do not contain any files or subdirectories.

**Removing an Empty Directory**: To remove an empty directory using rmdir, use the following syntax:

```
rmdir directoryname
```
This command removes the specified empty directory, indicated by "emptydirectory/."


**Removing Multiple Directories**: To remove multiple empty directories in one command, you can specify them all:

```
rmdir dir1/ dir2/ dir3/
```
This command removes "dir1," "dir2," and "dir3" if they are empty.

**Force Removal**: If you want to remove a directory and its parent directories, regardless of whether they are empty or not, you can use the rm -fr
```
rm -fr nonempty_directory
```
This option allows -fr to remove the specified directory even if it contains files or subdirectories.



[def]: image/list1.PNG
[def2]: #Working_With_Files_and_Directory