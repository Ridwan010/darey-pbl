### Pods in Minikube

#### Definition and Purpose:

In the Minikube environment, a **Pod** stands as the foundational deployment unit, representing one or more closely related containers that share the same network namespace and storage volumes. Essentially, a Pod is the smallest deployable entity in Kubernetes, encapsulating the basic building blocks of an application. These containers within a Pod collaborate and communicate seamlessly, fostering a cooperative environment. In Minikube, Pods serve as the primary unit for deploying, scaling, and managing applications.

#### Creating and Managing Pods:

Interaction with Pods in Minikube involves using the powerful `kubectl` command-line tool. You can engage in the following actions to create and manage Pods:


1. **List Pods:**
   ```
   kubectl get po -A
   ```
   This command provides an overview of the current status of Pods within the Minikube cluster.

![Get pods](minikube/kubectl-getpods.PNG)

2. **Inspect a Pod:**
   ```
   kubectl describe pod <pod-name>
   ```
   The command above can be used to gain detailed insights into a specific Pod, including events, container information, and overall configuration.


3. **Delete a Pod:**
   ```
   kubectl delete pod <pod-name>
   ```
   Removing a Pod from the Minikube cluster is as simple as issuing this command.


### Containers in Minikube

#### Definition and Purpose:

From our knowledge of docker, we know **Container** represents a lightweight, standalone, and executable software package that encapsulates everything needed to run a piece of software, including the code, runtime, libraries, and system tools. Containers are the fundamental units deployed within Pods, which are orchestrated by Kubernetes. In Minikube, containers play a central role in providing a consistent and portable environment for applications, ensuring they run reliably across various stages of the development lifecycle.



#### Integrating Containers into Pods:

 **Pod Definition with Containers:**
   In the Kubernetes world, containers come to life within Pods. Developers define a Pod YAML file that specifies the containers to run, their images, and other configuration details. This Pod becomes the unit of deployment, representing a cohesive application.

   Using `kubectl`, we can deploy Pods and, consequently, the containers within them to the Minikube cluster. This process ensures that the defined containers work in concert within the shared context of a Pod.

