## Deploying Applications in Minikube

In Kubernetes, deploying applications is a fundamental skill that every beginner needs to grasp. Deployment involves the process of taking your application code and running it on a Kubernetes cluster, ensuring that it scales, manages resources efficiently, and stays resilient. This hands-on project will guide you through deploying your first application using Minikube, a lightweight, single-node Kubernetes cluster perfect for beginners.

**Deployments in Kubernetes**
In Kubernetes, a **Deployment** is a declarative approach to managing and scaling applications. It provides a blueprint for the desired state of your application, allowing Kubernetes to handle the complexities of deploying and managing replicas. Whether you're running a simple web server or a more complex microservices architecture, Deployments are the cornerstone for maintaining application consistency and availability.

**Services in Kubernetes:**
Once your application is deployed, it needs a way to be accessed by other parts of your system or external users. This is where **Services** come into play. In Kubernetes, a Service is an abstraction that defines a logical set of Pods and a policy by which to access them. It acts as a stable endpoint to connect to your application, allowing for easy communication within the cluster or from external sources.

In subsequent sections, we will dive deep into deployment strategies and service configurations within the Kubernetes ecosystem, delving into the intricacies of these components to ensure a thorough understanding and proficiency in their utilization.


**Deploying a Minikube Sample Appliction**
```
kubectl create deployment hello-minikube --image=kicbase/echo-server:1.0
```
The command above creates a Kubernetes Deployment named "hello-minikube" running the "kicbase/echo-server:1.0" container image
```
kubectl expose deployment hello-minikube --type=NodePort --port=8080
```
The command above exposes the Kubernetes Deployment named "hello-minikube" as a NodePort service on port 8080
```
kubectl get services hello-minikube
```

![Deployment](minikube/deployment.PNG)

The easiest way to access this service is to let minikube launch a web browser for you

minikube service hello-minikube

<table>
  <tr>
    <td><img src="minikube/service1.PNG" alt="Image 1"></td>
    <td><img src="minikube/service2.PNG" alt="Image 2"></td>
  </tr>
</table>
