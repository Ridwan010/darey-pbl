## Setting Up A Minikube
Now that we have an idea of what kubernetes is, let's set up a minikube, but what is minikube ?

Minikube is an open-source tool that enables us to run Kubernetes clusters locally on their machines.As we now know that kubernetes is a container orchestration platform that automates the deployment, scaling, and management of containerized applications. Minikube simplifies the process of setting up a local Kubernetes environment for development and testing purposes.

Let's get start with setting up minikube.

### Prerequisites
- 2 CPUs or more
- 2GB of free memory
- 20GB of free disk space
- Container or virtual machine manager, such as: Docker, Hyper-V, VirtualBox, or VMware Fusion/Workstation
- Docker Desktop installed.

## Getting Started With Minikube

### Installing Minikube on Windows
To install minikube on windows, we need to use [chocolatey](https://chocolatey.org/). Chocolatey, jus like linux `apt` and `yum`, is a windows package manager for installing, updating and removing software packages on windows. 

**i.** Go to the windows search bar and launch a terminal with administrative access

![Launch Terminal](minikube/launch-terminal.PNG)

**ii.** Install Minikube
```
choco install minikube
```
![Install Minikube](minikube/win-installminikube.PNG)

**Note:** If you don't have [chocolatey](https://chocolatey.org/) installed, follow the [official documentation](https://chocolatey.org) to install it.

**iii.** Minikube needs docker as a driver and also to pull it's base image, therefore we need to install docker desktop for windows.

Go to [docker desktop](https://docs.docker.com/desktop/install/windows-install/) official documentation to install it if not installed

**iv.** Run the command below to start minikube using virtualbox as the driver
```
minikube start --driver=docker
```
![Minikube Start](minikube/win-minikubestart.PNG)


### Installing Minikube on Linux
For linux users, let's install minikube

**i.** Launch a terminal with administrative access

**ii.** We need to install docker as a driver for minikube and also for minikube to pull base images for the kubernetes cluster

```
sudo apt-get update
```
This a Linux command that refreshes the package list on a Debian-based system, ensuring the latest software information is available for installation.

```
sudo apt-get install ca-certificates curl gnupg
```
This a Linux command that installs essential packages including certificate authorities, a data transfer tool (curl), and the GNU Privacy Guard for secure communication and package verification.

```
sudo install -m 0755 -d /etc/apt/keyrings
```

The command above creates a directory (/etc/apt/keyrings) with specific permissions (0755) for storing keyring files, which are used for docker's authentication 

```
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```
This downloads the Docker GPG key using `curl`

```
sudo chmod a+r /etc/apt/keyrings/docker.gpg
```
Sets read permissions for all users on the Docker GPG key file within the APT keyring directory


**Let's add the repository to Apt sources**
```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
The "echo" command creates a Docker APT repository configuration entry for the Ubuntu system, incorporating the system architecture and Docker GPG key, and then "sudo tee /etc/apt/sources.list.d/docker.list > /dev/null" writes this configuration to the "/etc/apt/sources.list.d/docker.list" file.

```  
sudo apt-get update
```

- Install latest version of docker
```
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

- Verify that docker has been successfully installed
```
sudo systemctl status docker
```
![Docker status](minikube/docker-status.PNG)


**iii.** Install minikube

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
```
```
sudo dpkg -i minikube_latest_amd64.deb
```
The command above downloads minikube's binary and install minikube using dpkg

![install minikube](minikube/linux-installminikube.PNG)


**iv.** Start minikube
```
minikube start --driver=docker 
```

![Minikube Start](minikube/linux-minikubestart.PNG)

**v.** Kubectl is the command-line interface (CLI) tool for interacting with and managing Kubernetes clusters, allowing users to deploy, inspect, and manage applications within the Kubernetes environment. Let's install kubectl
```
sudo snap install kubectl --classic
```
This will download the kubernetes command line (kubectl) tool to interact with kubernetes cluster 

![Install Kubectl](minikube/install-kubectl.PNG)

### Installing Minikube on mac
For mac users, let's install minikube

**i.** Launch a terminal with administrative access

**ii.** Install minikube

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-darwin-amd64
```
```
sudo install minikube-darwin-amd64 /usr/local/bin/minikube

```
The command above downloads minikube's binary and install minikube.

**iv.** Just like windows and linux, we need docker desktop as a driver for minikube. TO install docker desktop for mac go to [docker desktop](https://docs.docker.com/desktop/install/mac-install/) official documentation to install it if not installed

**v.** Run the command below to start minikube using virtualbox as the driver
```
minikube start --driver=docker
```
