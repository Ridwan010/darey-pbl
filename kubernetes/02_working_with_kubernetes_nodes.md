### Kubernetes Nodes in Minikube
Now that we have our minikube cluster setup, let's dive into nodes in kubernetes

#### What Is a Node

In the context of Minikube, a **Kubernetes Node** is a physical or virtual machine that runs the Kubernetes software and serves as a worker machine in the cluster. Nodes are responsible for running Pods, which are the basic deployable units in Kubernetes. Each node in a Minikube cluster typically represents a single host system.

#### Managing Nodes in Minikube:

Minikube simplifies the management of Kubernetes nodes for development and testing purposes. Here are common tasks related to managing nodes in Minikube:

1. **Start Minikube Cluster:**
   ```
   minikube start
   ```
   This command starts a local Kubernetes cluster using a single-node Minikube setup. It provisions a virtual machine (VM) as the Kubernetes node.

2. **Stop Minikube Cluster:**
   ```
   minikube stop
   ```
   Stops the running Minikube cluster, preserving the cluster state.

3. **Delete Minikube Cluster:**
   ```
   minikube delete
   ```
   Deletes the Minikube cluster and its associated resources.

4. **View Nodes:**
   ```
   kubectl get nodes
   ```
   ![Get Nodes](minikube/kubectl-getnodes.PNG)

   Lists all the nodes in the Minikube cluster along with their current status.

5. **Inspect a Node:**
   ```
   kubectl describe node <node-name>
   ```


   Provides detailed information about a specific node, including its capacity, allocated resources, and status.
 
 ![Describe Node](minikube/kubectl-describenode.PNG)

#### Node Scaling and Maintenance:

In Minikube, as it's often used for local development and testing, scaling nodes may not be as critical as in production environments. However, understanding the concepts is beneficial:

- **Node Scaling:** Minikube is typically a single-node cluster, meaning you have one worker node. For larger, production-like environments, you might consider using tools like Kubeadm for multi-node setups.

- **Node Upgrades:** Minikube allows you to easily upgrade your local cluster to a new Kubernetes version, ensuring that your development environment aligns with the target production version.

By effectively managing nodes in Minikube, developers can create, test, and deploy applications locally, simulating a Kubernetes cluster without the need for a full-scale production setup. This is particularly useful for debugging, experimenting, and developing applications in a controlled environment.